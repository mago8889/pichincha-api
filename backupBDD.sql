PGDMP     /    4                z         	   pichincha    11.10    13.4                0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false                        0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            !           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            "           1262    28454 	   pichincha    DATABASE     g   CREATE DATABASE pichincha WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Spanish_Ecuador.1252';
    DROP DATABASE pichincha;
                postgres    false            �            1259    28515 
   seqcliente    SEQUENCE     s   CREATE SEQUENCE public.seqcliente
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 !   DROP SEQUENCE public.seqcliente;
       public          postgres    false            �            1259    28517 	   seqcuenta    SEQUENCE     r   CREATE SEQUENCE public.seqcuenta
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
     DROP SEQUENCE public.seqcuenta;
       public          postgres    false            �            1259    28519    seqmovimiento    SEQUENCE     v   CREATE SEQUENCE public.seqmovimiento
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.seqmovimiento;
       public          postgres    false            �            1259    28521 
   seqpersona    SEQUENCE     s   CREATE SEQUENCE public.seqpersona
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 !   DROP SEQUENCE public.seqpersona;
       public          postgres    false            �            1259    28570    tbl_clientes    TABLE     �   CREATE TABLE public.tbl_clientes (
    id bigint NOT NULL,
    contrasena character varying(255) NOT NULL,
    estado boolean NOT NULL,
    id_persona bigint NOT NULL
);
     DROP TABLE public.tbl_clientes;
       public            postgres    false            �            1259    28575    tbl_cuentas    TABLE        CREATE TABLE public.tbl_cuentas (
    id bigint NOT NULL,
    estado boolean NOT NULL,
    numero_cuenta character varying(255) NOT NULL,
    saldo_inicial bigint NOT NULL,
    tipo_cuenta character varying(255) NOT NULL,
    id_cliente bigint NOT NULL
);
    DROP TABLE public.tbl_cuentas;
       public            postgres    false            �            1259    28583    tbl_movimientos    TABLE     �   CREATE TABLE public.tbl_movimientos (
    id bigint NOT NULL,
    fecha timestamp without time zone NOT NULL,
    saldo bigint NOT NULL,
    tipo_movimiento character varying(255) NOT NULL,
    valor bigint NOT NULL,
    id_cuenta bigint NOT NULL
);
 #   DROP TABLE public.tbl_movimientos;
       public            postgres    false            �            1259    28588    tbl_personas    TABLE     A  CREATE TABLE public.tbl_personas (
    id bigint NOT NULL,
    direccion character varying(255) NOT NULL,
    edad integer NOT NULL,
    genero character varying(255) NOT NULL,
    identificacion character varying(255) NOT NULL,
    nombre character varying(255) NOT NULL,
    telefono character varying(255) NOT NULL
);
     DROP TABLE public.tbl_personas;
       public            postgres    false                      0    28570    tbl_clientes 
   TABLE DATA           J   COPY public.tbl_clientes (id, contrasena, estado, id_persona) FROM stdin;
    public          postgres    false    200                    0    28575    tbl_cuentas 
   TABLE DATA           h   COPY public.tbl_cuentas (id, estado, numero_cuenta, saldo_inicial, tipo_cuenta, id_cliente) FROM stdin;
    public          postgres    false    201   K                 0    28583    tbl_movimientos 
   TABLE DATA           ^   COPY public.tbl_movimientos (id, fecha, saldo, tipo_movimiento, valor, id_cuenta) FROM stdin;
    public          postgres    false    202   �                 0    28588    tbl_personas 
   TABLE DATA           e   COPY public.tbl_personas (id, direccion, edad, genero, identificacion, nombre, telefono) FROM stdin;
    public          postgres    false    203   p        #           0    0 
   seqcliente    SEQUENCE SET     8   SELECT pg_catalog.setval('public.seqcliente', 7, true);
          public          postgres    false    196            $           0    0 	   seqcuenta    SEQUENCE SET     8   SELECT pg_catalog.setval('public.seqcuenta', 15, true);
          public          postgres    false    197            %           0    0    seqmovimiento    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.seqmovimiento', 9, true);
          public          postgres    false    198            &           0    0 
   seqpersona    SEQUENCE SET     8   SELECT pg_catalog.setval('public.seqpersona', 7, true);
          public          postgres    false    199            �
           2606    28574    tbl_clientes tbl_clientes_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.tbl_clientes
    ADD CONSTRAINT tbl_clientes_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.tbl_clientes DROP CONSTRAINT tbl_clientes_pkey;
       public            postgres    false    200            �
           2606    28582    tbl_cuentas tbl_cuentas_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.tbl_cuentas
    ADD CONSTRAINT tbl_cuentas_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.tbl_cuentas DROP CONSTRAINT tbl_cuentas_pkey;
       public            postgres    false    201            �
           2606    28587 $   tbl_movimientos tbl_movimientos_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.tbl_movimientos
    ADD CONSTRAINT tbl_movimientos_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.tbl_movimientos DROP CONSTRAINT tbl_movimientos_pkey;
       public            postgres    false    202            �
           2606    28595    tbl_personas tbl_personas_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.tbl_personas
    ADD CONSTRAINT tbl_personas_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.tbl_personas DROP CONSTRAINT tbl_personas_pkey;
       public            postgres    false    203            �
           2606    28601 '   tbl_cuentas fkdmidgh08li62p2vs59litfbes    FK CONSTRAINT     �   ALTER TABLE ONLY public.tbl_cuentas
    ADD CONSTRAINT fkdmidgh08li62p2vs59litfbes FOREIGN KEY (id_cliente) REFERENCES public.tbl_clientes(id);
 Q   ALTER TABLE ONLY public.tbl_cuentas DROP CONSTRAINT fkdmidgh08li62p2vs59litfbes;
       public          postgres    false    201    200    2706            �
           2606    28596 (   tbl_clientes fkf20myo6xnu9g9lqp4qtfeeyif    FK CONSTRAINT     �   ALTER TABLE ONLY public.tbl_clientes
    ADD CONSTRAINT fkf20myo6xnu9g9lqp4qtfeeyif FOREIGN KEY (id_persona) REFERENCES public.tbl_personas(id);
 R   ALTER TABLE ONLY public.tbl_clientes DROP CONSTRAINT fkf20myo6xnu9g9lqp4qtfeeyif;
       public          postgres    false    200    2712    203            �
           2606    28606 *   tbl_movimientos fklg2sr9a9f7i51heo1om4jexe    FK CONSTRAINT     �   ALTER TABLE ONLY public.tbl_movimientos
    ADD CONSTRAINT fklg2sr9a9f7i51heo1om4jexe FOREIGN KEY (id_cuenta) REFERENCES public.tbl_cuentas(id);
 T   ALTER TABLE ONLY public.tbl_movimientos DROP CONSTRAINT fklg2sr9a9f7i51heo1om4jexe;
       public          postgres    false    2708    202    201               *   x�3�4426�,�4�2�453� 2͸́�&�@�9W� |y(         c   x�U�A
�@D�u�0C�'Չ��k�t������z�R� �4BK)��m���AQM4g@��)u����J���&�����Y�<��[Z��o2|D����         �   x�m�1�0�N����V���U���?G�,M�Hf0z>���@i�t�T�J����>�����f�K�{PV��
��j����,#j/S��L�V�z�{�9m��a:b���6E�i��
��m��Rz�����y��h�u��i�}Z�|)�Z�D߀�?i�I�         �   x�eͱ�0 ����JK����\.����!�zar�^��n���`b|�4�D
�0�t�ʠ�4�m|d���8oU	�'}�)��m��Pןʡ�9���^(n�sg��J��>V���k,��n�J�]�9�{g�w�~RJ}3�     