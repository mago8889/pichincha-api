package com.pichincha.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pichincha.models.Cliente;
import com.pichincha.services.ClienteService;

@RestController
@RequestMapping("/api")
public class ClienteController {

	private final ClienteService clienteService;
	
	public ClienteController(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	
	@Async
	@GetMapping("/getCliente/{id}")
	public Optional<Cliente> getCliente(@PathVariable Long id) throws Exception {
		
		var cliente = clienteService.getCliente(id);
		return cliente;
	}
	
	@Async
	@GetMapping("/getClientes")
	public List<Cliente> getClientes() throws Exception {
		
		var clientes = clienteService.getClientes();
		return clientes;
	}
	
	@Async
	@PostMapping("/postCliente")
	public Cliente postCliente(@RequestBody Cliente cliente) {
		var data = clienteService.postCliente(cliente);
		return data;
	}
	
	@Async
	@PutMapping("/putCliente")
	public Cliente putCliente(@RequestBody Cliente cliente) {
		var data = clienteService.postCliente(cliente);
		return data;
	}
	
	@Async
	@DeleteMapping("/deleteCliente/{id}")
	public Cliente deleteCliente(@PathVariable Long id) {
		var data = clienteService.deleteCliente(id);
		return data;
	}
	
}
