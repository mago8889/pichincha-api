package com.pichincha.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pichincha.models.Cuenta;
import com.pichincha.services.CuentaService;

@RestController
@RequestMapping("/api")
public class CuentaController {

	private final CuentaService cuentaService;
	
	public CuentaController (CuentaService cuentaService) {
		this.cuentaService = cuentaService;
	}
	
	@Async
	@GetMapping("/getCuenta/{id}")
	public Optional<Cuenta> getCuenta(@PathVariable Long id) {
		
		var data = cuentaService.getCuenta(id);
		return data;
	} 
	
	@Async
	@GetMapping("/getCuentas")
	public List<Cuenta> getCuentas(){
		var data = cuentaService.getCuentas();
		return data;
	}
	
	@Async
	@PostMapping("/postCuenta")
	public Cuenta postCuenta(@RequestBody Cuenta cuenta) {
		var data = cuentaService.postCuenta(cuenta);
		return data;
	}
	
	@Async
	@PutMapping("/putCuenta")
	public Cuenta putCuenta(@RequestBody Cuenta cuenta) {
		var data = cuentaService.putCuenta(cuenta);
		return data;
	}
	
	@Async
	@DeleteMapping("/deleteCuenta/{id}")
	public Cuenta deleteCuenta(@PathVariable Long id) {
		var data = cuentaService.deleteCuenta(id);
		return data;
	}
}
