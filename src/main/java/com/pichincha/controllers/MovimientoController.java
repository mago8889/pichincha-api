package com.pichincha.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pichincha.models.Movimiento;
import com.pichincha.services.MovimientoService;
import com.pichincha.utils.DataConsulta;

@RestController
@RequestMapping("/api")
public class MovimientoController {

	private final MovimientoService movimientoService;
	
	public MovimientoController(MovimientoService movimientoService) {
		this.movimientoService = movimientoService;
	}
	
	@Async
	@GetMapping("/getMovimiento/{id}")
	public Optional<Movimiento> getMovimiento(@PathVariable Long id) {
		var data = movimientoService.getMovimiento(id);
		return data;
	}
	
	@Async
	@GetMapping("/getMovimientos")
	public List<Movimiento> getMovimientos(){
		var data = movimientoService.getMovimientos();
		return data;
	}
	
	@Async
	@PostMapping("/postMovimiento")
	public Movimiento postMovimiento(@RequestBody Movimiento movimiento) {
		var data = movimientoService.postMovimiento(movimiento);
		return data;
	}
	
	@Async
	@PutMapping("/putMovimiento")
	public Movimiento putMovimiento(@RequestBody Movimiento movimiento) throws Exception {
		var data = movimientoService.putMovimiento(movimiento);
		return data;
	}
	
	@Async
	@DeleteMapping("/deleteMovimiento/{id}")
	public Movimiento deleteMovimiento (@PathVariable Long id) throws Exception {
		var data = movimientoService.deleteMovimiento(id);
		return data;
	}
	
	@Async
	@GetMapping("/rptMovimientos")
	public Object rptMovimientos(@RequestBody DataConsulta consulta) {
		var data = movimientoService.reporteMovimientos(consulta);
		return data;
	}
}
