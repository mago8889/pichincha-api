package com.pichincha.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="tbl_cuentas")
public class Cuenta {

	@Id
	@SequenceGenerator(name = "SEQCUENTA", sequenceName = "SEQCUENTA", allocationSize = 1) 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQCUENTA")
	@Column(name="id")
	private Long idCuenta;
	
	@Column(name="numero_cuenta", nullable=false)
	private String numeroCuenta;
	
	@Column(name="tipo_cuenta", nullable=false)
	private String tipoCuenta;
	
	@Column(name="saldo_inicial", nullable=false)
	private Long saldoInicial;
	
	@Column(name="estado", nullable=false)
	private Boolean estado;
	
	@ManyToOne
    @JoinColumn(name="id_cliente", nullable=false)
	private Cliente cliente;

	public Long getIdCuenta() {
		return idCuenta;
	}

	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public String getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public Long getSaldoInicial() {
		return saldoInicial;
	}

	public void setSaldoInicial(Long saldoInicial) {
		this.saldoInicial = saldoInicial;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
}
