package com.pichincha.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="tbl_personas")
public class Persona {
	
	@Id
	@SequenceGenerator(name = "SEQPERSONA", sequenceName = "SEQPERSONA", allocationSize = 1) 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQPERSONA")
	@Column(name="id")
	private Long idPersona;
	
	@Column(name="nombre", nullable=false)
	private String nombre;
	
	@Column(name="genero", nullable=false)
	private String genero;
	
	@Column(name="edad", nullable=false)
	private int edad;
	
	@Column(name="identificacion", nullable=false)
	private String identificacion;
	
	@Column(name="direccion", nullable=false)
	private String direccion;
	
	@Column(name="telefono", nullable=false)
	private String telefono;

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

}
