package com.pichincha.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="tbl_movimientos")
public class Movimiento {

	@Id
	@SequenceGenerator(name = "SEQMOVIMIENTO", sequenceName = "SEQMOVIMIENTO", allocationSize = 1) 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQMOVIMIENTO")
	@Column(name="id")
	private Long idMovimiento;
	
	@Column(name="fecha", nullable=false)
	private Date fecha;
	
	@Column(name="tipo_movimiento", nullable=false)
	private String tipoMovimiento;
	
	@Column(name="valor", nullable=false)
	private Long valor;
	
	@Column(name="saldo", nullable=false)
	private Long saldo;
	
	@ManyToOne
    @JoinColumn(name="id_cuenta", nullable=false)
	private Cuenta cuenta;

	public Long getIdMovimiento() {
		return idMovimiento;
	}

	public void setIdMovimiento(Long idMovimiento) {
		this.idMovimiento = idMovimiento;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	public Long getValor() {
		return valor;
	}

	public void setValor(Long valor) {
		this.valor = valor;
	}

	public Long getSaldo() {
		return saldo;
	}

	public void setSaldo(Long saldo) {
		this.saldo = saldo;
	}

	public Cuenta getCuenta() {
		return cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}

}
