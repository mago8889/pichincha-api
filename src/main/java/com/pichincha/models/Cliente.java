package com.pichincha.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="tbl_clientes")
public class Cliente {
	
	@Id
	@SequenceGenerator(name = "SEQCLIENTE", sequenceName = "SEQCLIENTE", allocationSize = 1) 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQCLIENTE")
	@Column(name="id")
	private Long idCliente;
	
	@Column(name="contrasena", nullable=false)
	private String contrasena;
	
	@Column(name="estado", nullable=false)
	private Boolean estado;
	
	@ManyToOne
    @JoinColumn(name="id_persona", nullable=false)
	private Persona persona;

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}	
	
}
