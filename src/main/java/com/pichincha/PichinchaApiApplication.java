package com.pichincha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PichinchaApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PichinchaApiApplication.class, args);
	}

}
