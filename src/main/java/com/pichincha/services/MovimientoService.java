package com.pichincha.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.pichincha.models.Cuenta;
import com.pichincha.models.Movimiento;
import com.pichincha.repository.CuentaRepository;
import com.pichincha.repository.MovimientoRepository;
import com.pichincha.utils.DataConsulta;
import com.pichincha.utils.RptMovimiento;


@Service
public class MovimientoService {
	
	private final MovimientoRepository movimientoRepository;
	private final CuentaRepository cuentaRepository;
	
	public MovimientoService (MovimientoRepository movimientoRepository, CuentaRepository cuentaRepository) {
		this.movimientoRepository = movimientoRepository;
		this.cuentaRepository = cuentaRepository;
	}
	
	public Optional<Movimiento> getMovimiento(Long id) {
		var data = movimientoRepository.findById(id);
		return data;
	}
	
	public List<Movimiento> getMovimientos(){
		var data = movimientoRepository.findAll();
		return (List<Movimiento>) data;
	}
	
	public Movimiento postMovimiento (Movimiento movimiento) {
		Optional<Cuenta> cuenta = cuentaRepository.findById(movimiento.getCuenta().getIdCuenta());
		var saldoActual = cuenta.get().getSaldoInicial();
		var saldo = saldoActual + movimiento.getValor();
		
		movimiento.setSaldo(saldo);
		movimiento.setTipoMovimiento(movimiento.getValor() > 0 ? "DEPOSITO" : "RETIRO");		
		var data = movimientoRepository.save(movimiento);
		
		cuenta.get().setSaldoInicial(saldo);
		cuentaRepository.save(cuenta.get());
		
		return data;
	}
	
	public Movimiento putMovimiento(Movimiento movimiento) throws Exception {
		throw new Exception("No implementado");
	}
	
	public Movimiento deleteMovimiento(Long id) throws Exception {
		throw new Exception("No implementado");
	}
	
	public Object reporteMovimientos(DataConsulta dataConsulta) {
		
		List<Movimiento> movimientos = new ArrayList<Movimiento>();
		List<RptMovimiento> rptMovimientos = new ArrayList<RptMovimiento>();
		
		movimientos = (List<Movimiento>) movimientoRepository.findAll();
		
		for(Movimiento i: movimientos) {			
			if(i.getCuenta().getCliente().getIdCliente().equals(dataConsulta.getIdCliente())) {
				RptMovimiento movimiento = new RptMovimiento();
				movimiento.setEstado(i.getCuenta().getEstado());
				movimiento.setFecha(i.getFecha());
				movimiento.setMovimiento(i.getValor());
				movimiento.setNombre(i.getCuenta().getCliente().getPersona().getNombre());
				movimiento.setNumeroCuenta(i.getCuenta().getNumeroCuenta());
				movimiento.setSaldoDsiponible(i.getSaldo());
				movimiento.setSaldoInicial(i.getCuenta().getSaldoInicial());
				movimiento.setTipo(i.getTipoMovimiento());
				
				rptMovimientos.add(movimiento);
			}			
		}
		
		return rptMovimientos;
		
	}

}
