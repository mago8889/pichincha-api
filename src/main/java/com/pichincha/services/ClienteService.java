package com.pichincha.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.pichincha.models.Cliente;
import com.pichincha.repository.ClienteRepository;
import com.pichincha.repository.PersonaRepository;

@Service
public class ClienteService {
	
	private final ClienteRepository clienteRepository;
	private final PersonaRepository personaRepository;
	
	public ClienteService (ClienteRepository clienteRepository, PersonaRepository personaRepository) {
		this.clienteRepository = clienteRepository;
		this.personaRepository = personaRepository;
	}
	
	public Optional<Cliente> getCliente(Long id) throws Exception {
		try {
			var data = clienteRepository.findById(id);		
			return data;
		} catch (Exception e) {
			throw new Exception(e);
		}
		
	}
	
	public List<Cliente> getClientes() throws Exception{
		try {
			var data = clienteRepository.findAll();
			return (List<Cliente>) data;
		} catch (Exception e) {
			throw new Exception(e);
		}
		
	}
	
	public Cliente postCliente(Cliente cliente) {
		var dataPersona = personaRepository.save(cliente.getPersona());
		Cliente newCliente = new Cliente();
		newCliente = cliente;
		newCliente.setPersona(dataPersona);
		var data = clienteRepository.save(newCliente);
		return data;
	}
	
	public Cliente putCliente(Cliente cliente) {
		var dataPersona = personaRepository.save(cliente.getPersona());
		Cliente newCliente = new Cliente();
		newCliente = cliente;
		newCliente.setPersona(dataPersona);
		var data = clienteRepository.save(newCliente);
		return data;
	}
	
	public Cliente deleteCliente(Long id) {
		Optional<Cliente> cliente = clienteRepository.findById(id);
		cliente.get().setEstado(false);
		var data = clienteRepository.save(cliente.get());
		return data;
	}


}
