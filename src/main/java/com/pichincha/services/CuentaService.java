package com.pichincha.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.pichincha.models.Cuenta;
import com.pichincha.models.Movimiento;
import com.pichincha.repository.CuentaRepository;
import com.pichincha.repository.MovimientoRepository;

@Service
public class CuentaService {
	
	private final CuentaRepository cuentaRepository;
	private final MovimientoRepository movimientoRepository;
		
	public CuentaService (CuentaRepository cuentaRepository, MovimientoRepository movimientoRepository) {
		this.cuentaRepository = cuentaRepository;
		this.movimientoRepository = movimientoRepository;
	}
	
	public Optional<Cuenta> getCuenta(Long id) {
		var data = cuentaRepository.findById(id);
		return data;
	}
	
	public List<Cuenta> getCuentas (){
		var data = cuentaRepository.findAll();
		return (List<Cuenta>) data;
	}
	
	public Cuenta postCuenta(Cuenta cuenta) {
		var data = cuentaRepository.save(cuenta);
		
		Movimiento movimiento = new Movimiento();
		movimiento.setCuenta(data);
		movimiento.setFecha(new Date());
		movimiento.setSaldo(cuenta.getSaldoInicial());
		movimiento.setTipoMovimiento("CREACION");
		movimiento.setValor((long) 0);
		
		movimientoRepository.save(movimiento);
		
		return data;
	}
	
	public Cuenta putCuenta(Cuenta cuenta) {
		var data = cuentaRepository.save(cuenta);
		return data;
	}
	
	public Cuenta deleteCuenta(Long id) {
		Optional<Cuenta> cuenta = cuentaRepository.findById(id);
		cuenta.get().setEstado(false);
		var data = cuentaRepository.save(cuenta.get());
		return data;
	}

}
