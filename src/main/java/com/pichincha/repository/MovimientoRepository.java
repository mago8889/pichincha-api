package com.pichincha.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pichincha.models.Movimiento;

@Repository
public interface MovimientoRepository extends CrudRepository<Movimiento, Long> {

}
