package com.pichincha.repository;

import org.springframework.data.repository.CrudRepository;

import com.pichincha.models.Persona;

public interface PersonaRepository extends CrudRepository<Persona, Long> {

}
