package com.pichincha.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pichincha.models.Cliente;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente,Long> {
	
}
