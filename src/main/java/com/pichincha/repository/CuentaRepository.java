package com.pichincha.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pichincha.models.Cuenta;

@Repository
public interface CuentaRepository extends CrudRepository<Cuenta, Long> {

}
