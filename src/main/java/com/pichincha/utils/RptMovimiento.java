package com.pichincha.utils;

import java.util.Date;

public class RptMovimiento {
	
	private Date fecha;
	private String nombre;
	private String numeroCuenta;
	private String Tipo;
	private Long saldoInicial;
	private Boolean estado;
	private Long movimiento;
	private Long saldoDsiponible;
	
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	public String getTipo() {
		return Tipo;
	}
	public void setTipo(String tipo) {
		Tipo = tipo;
	}
	public Long getSaldoInicial() {
		return saldoInicial;
	}
	public void setSaldoInicial(Long saldoInicial) {
		this.saldoInicial = saldoInicial;
	}
	public Boolean getEstado() {
		return estado;
	}
	public void setEstado(Boolean estado) {
		this.estado = estado;
	}
	public Long getMovimiento() {
		return movimiento;
	}
	public void setMovimiento(Long movimiento) {
		this.movimiento = movimiento;
	}
	public Long getSaldoDsiponible() {
		return saldoDsiponible;
	}
	public void setSaldoDsiponible(Long saldoDsiponible) {
		this.saldoDsiponible = saldoDsiponible;
	}

}
